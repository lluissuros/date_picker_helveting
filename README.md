date-picker helveting
=========================

 This is an assessment solution for the Helveting interview.
 Basically these are modifications on https://github.com/Syncofy/calendar-date-picker



### Options
* `orientation`: (string)horizontal/vertical.
* `selectedDate`: (string) The selected date.
* `opened`: (boolean) Whether the date picker elements are currently showing.

### Methods
* `toogle`: Toggle the date picker `opened` attribute.
* `open` : Open the date picker.
